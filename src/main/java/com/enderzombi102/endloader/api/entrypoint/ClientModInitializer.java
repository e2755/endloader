package com.enderzombi102.endloader.api.entrypoint;

public interface ClientModInitializer {
	void onClientInitialization();
}

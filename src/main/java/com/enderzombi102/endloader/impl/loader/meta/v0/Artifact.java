package com.enderzombi102.endloader.impl.loader.meta.v0;

import com.enderzombi102.endloader.api.loader.meta.Dependable;
import com.enderzombi102.endloader.impl.loader.discovery.LoadEnvironment;

public record Artifact(
		String name,
		String artifact,
		String repository
) implements Dependable {
	@Override
	public boolean acceptsEnvironment( LoadEnvironment environment ) {
		return true;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getVersion() {
		return this.artifact.split(":")[2];
	}

	@Override
	public String toString() {
		return "Artifact{name='$name', artifact='$artifact', repository='$repository'}";
	}
}

package com.enderzombi102.endloader.api.loader.container;

import com.enderzombi102.endloader.api.ModClassLoader;
import com.enderzombi102.endloader.api.loader.meta.ModMetadata;

import java.nio.file.Path;
import java.util.List;

public interface ModContainer {
	boolean hasBeenUpdated();
	Path getJarPath();
	Path getPath( String part );
	ModMetadata getMetadata();
	<T> List<T> getEntrypoints( Class<T> entrypointClass, String name );
	ModClassLoader getModClassLoader();
}

package com.enderzombi102.endloader.test.adapter;

import com.enderzombi102.endloader.api.loader.container.ModContainer;
import com.enderzombi102.endloader.api.loader.adapter.LanguageAdapter;
import com.enderzombi102.endloader.api.loader.adapter.LanguageAdapterException;

public class PythonAdapter implements LanguageAdapter {
	@Override
	public <T> T findEntrypoint(Class<T> entrypointType, ModContainer container, String qualifiedName) throws LanguageAdapterException {
		throw new LanguageAdapterException( "Entrypoint $qualifiedName from ${container.getMetadata().getId()} was not found!" );
	}
}

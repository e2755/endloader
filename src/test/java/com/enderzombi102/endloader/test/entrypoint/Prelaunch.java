package com.enderzombi102.endloader.test.entrypoint;

import com.enderzombi102.endloader.api.entrypoint.PreLaunchEntrypoint;
import org.slf4j.LoggerFactory;

public class Prelaunch implements PreLaunchEntrypoint {
	@Override
	public void onPreLaunch() {
		LoggerFactory.getLogger( "Test Mod | " + this.getClass().getSimpleName() ).info("Hello world!");
	}
}

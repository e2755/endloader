package com.enderzombi102.endloader.impl.loader.discovery;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarFile;

public class ModFinder {
	private final List<ModDiscoverer> discoverers = new ArrayList<>();

	public ModFinder() {
		discoverers.add( this::findModsInClasspath );
		discoverers.add( this::findModsInDirectory );
		discoverers.add( this::findModsInVersioned );
	}

	public List<Path> findMods( LoadEnvironment env ) {
		return discoverers
			// first make the discoverers find the jars
			.flatMap( discoverer -> discoverer.findCandidates( env ) )
			.filter( path -> {
				// then add to the list only those with a `endloader.json5` file
				try ( var jar = new JarFile( path.toFile() ) ) {
					return jar.getJarEntry( "endloader.json5" ) != null;
				}
			} ).toList().immutable();
	}

	public List<Path> findModsInVersioned( LoadEnvironment env ) {
		return findModsInDirectory( new LoadEnvironment( env.devEnv(), env.modsDir().resolve( env.gameVersion().toString() ), env.gameVersion() ) );
	}

	@SuppressWarnings("resource")
	public List<Path> findModsInDirectory( LoadEnvironment env ) {
		//noinspection ResultOfMethodCallIgnored
		env.modsDir().toFile().mkdirs();

		if ( env.modsDir().toFile().exists() )
			return Files.list( env.modsDir() )
					.filter( path -> path.toFile().isFile() && path.toFile().getExtension().equals("jar") )
					.toList();
		return List.of();
	}

	private List<Path> findModsInClasspath( LoadEnvironment env ) {
		return List.of();
	}
}

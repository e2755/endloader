package com.enderzombi102.endloader.impl.loader.launch;

import com.enderzombi102.endloader.api.EndLoader;
import com.enderzombi102.endloader.impl.Const;
import com.enderzombi102.endloader.impl.loader.EndLoaderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EndMain {
	private static final Logger LOGGER = LoggerFactory.getLogger("EndLoader | Main");

	public static void main( String[] argv ) {
		// check for relauncher flag, this is so this stuff is done only once at initial startup
		if ( System.getProperty("loader.relauncher.done") == null ) {
			LOGGER.info(
				"Starting EndLoader {} for minecraft {} in {} mode",
				EndLoader.getLoaderContainer().getMetadata().getVersion(),
				EndLoader.getMinecraftContainer().getMetadata().getVersion(),
				Const.DEV_ENV ? "dev" : "release"
			);
			// initialize loader
			EndLoaderImpl.INSTANCE.findMods();
			// is relaunching allowed?
			if ( !Boolean.parseBoolean( System.getProperty("loader.relauncher.disabled") ) ) {
				// yes, we should relaunch
				var proc = new Object() {
					public final int exitCode = 0;
				};
				System.exit(proc.exitCode);
			}
		}
		// if present, start game
//		LOGGER.info( EndLoader.getJavaContainer().toString() );
	}
}

package com.enderzombi102.endloader.test.mixin;

import net.minecraft.client.gui.screen.TitleScreen;
import org.slf4j.LoggerFactory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TitleScreen.class)
public class TitleScreenMixin {
	@Inject( method = "init", at = @At( "TAIL" ) )
	public void onInit(CallbackInfo ci) {
		LoggerFactory.getLogger("Test Mod | TitleScreenMixin").info("Hello world!");
	}
}

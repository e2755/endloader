package com.enderzombi102.endloader.api.entrypoint;

import com.enderzombi102.endloader.api.loader.container.ModContainer;

public interface ModInitializer {
	/**
	 * Executed on mod initialization.
	 * @param container the mod's container
	 */
	void onInitialize( ModContainer container );
}

package com.enderzombi102.endloader.impl.loader.meta.v0;

import com.enderzombi102.endloader.api.loader.meta.Dependable;
import com.enderzombi102.endloader.api.loader.meta.InvalidMetadataException;
import com.enderzombi102.endloader.api.loader.meta.ModMetadata;
import com.enderzombi102.endloader.impl.loader.meta.Environment;
import com.enderzombi102.endloader.impl.loader.meta.License;
import com.github.zafarkhaja.semver.Version;
import org.quiltmc.json5.JsonReader;
import org.quiltmc.json5.JsonToken;

import java.io.InputStream;
import java.util.*;

public final class ModMetadataParser {
	public static ModMetadata readMetadata( InputStream stream ) {
		return parseMetadata( new String( stream.readAllBytes() ) );
	}

	public static ModMetadata parseMetadata( String meta ) {
		var metadata = new ModMetadataImpl();
		// defaults
		metadata.environment = Environment.BOTH;
		metadata.language = "java";
		metadata.mixins = List.of();
		metadata.dependencies = List.of();
		metadata.entrypoints = Map.of();
		// read properties
		try ( var reader =  JsonReader.json5( meta ) ) {
			reader.beginObject();
			while ( reader.hasNext() ) {
				switch ( reader.nextName() ) {
					// schema must be of version 0
					case "schemaVersion" -> {
						var ver = reader.nextInt();
						if ( ver != 0 )
							throw new InvalidMetadataException("Invalid schema version \"$ver\"!");
					}
					case "modid" -> metadata.id = reader.nextString();
					case "name" -> metadata.name = reader.nextString();
					case "authors" -> {
						var authors = new ArrayList<String>();
						reader.beginArray();
							while ( reader.hasNext() )
								authors.add( reader.nextString() );
						reader.endArray();
						metadata.authors = authors.immutable();
					}
					case "description" -> metadata.description = reader.nextString();
					case "version" -> {
						var string = reader.nextString();
						metadata.version = Version.valueOf( string.startsWith("${") ? "0.1.0" : string );
					}
					case "environment" -> metadata.environment = Environment.of( reader.nextString() );
					case "parent" -> metadata.parent = reader.nextString();
					case "language" -> metadata.language = reader.nextString();
					case "declare" -> {
						var entrypoints = new HashMap<String, String>();
						var adapters = new HashMap<String, String>();
						var layers = new ArrayList<String>();
						reader.beginObject();
							while ( reader.hasNext() ) {
								var name = reader.nextName();
								switch ( name ) {
									case "languageAdapters" -> {
										reader.beginObject();
										while ( reader.hasNext() ) {
											adapters.put( reader.nextName(), reader.nextString() );
										}
										reader.endObject();
									}
									case "entrypoints" -> {
										reader.beginObject();
										while ( reader.hasNext() ) {
											entrypoints.put( reader.nextName(), reader.nextString() );
										}
										reader.endObject();
									}
									case "layers" -> {
										reader.beginArray();
										while ( reader.hasNext() )
											layers.add( reader.nextString() );
										reader.endArray();
									}
									default -> throw new InvalidMetadataException("Invalid declare subkey \"$name\"!");
								}
							}
						reader.endObject();
						metadata.declare = new DeclareMetaImpl( entrypoints, adapters, layers );
					}
					case "entrypoints" -> {
						var entrypoints = new HashMap< String, List<String> >();
						reader.beginObject();
							while ( reader.hasNext() ) {
								var name = reader.nextName();
								var list = new ArrayList<String>();
								reader.beginArray();
									while ( reader.hasNext() )
										list.add( reader.nextString() );
								reader.endArray();
								entrypoints.put( name,  list.immutable() );
							}
						reader.endObject();
						metadata.entrypoints =  entrypoints.immutable();
					}
					case "dependencies" -> {
						var deps = new ArrayList<Dependable>();
						reader.beginObject();
							while ( reader.hasNext() ) {
								var name = reader.nextName();
								if ( reader.peek() == JsonToken.BEGIN_OBJECT ) {
									String artifact = null;
									String maven = "mavenCentral";
									reader.beginObject();
										while ( reader.hasNext() ) {
											var key = reader.nextName();
											switch ( key ) {
												case "artifact" -> artifact = reader.nextString();
												case "maven" -> maven = reader.nextString();
												default -> throw new InvalidMetadataException("Invalid artifact subkey \"$name\"!");
											}
										}
									reader.endObject();
									if ( artifact == null )
										throw new InvalidMetadataException("Missing key \"artifact\" in artifact dependency $name!");
									deps.add( new Artifact( name, artifact, maven ) );
								} else {
									deps.add( new Dependency( name, reader.nextString() ) );
								}
							}
						reader.endObject();
						metadata.dependencies =  deps.immutable();
					}
					case "links" -> {
						var links = new HashMap<String, String>();
						reader.beginObject();
							while ( reader.hasNext() ) {
								links.put( reader.nextName(), reader.nextString() );
							}
						reader.endObject();
						metadata.links =  links.immutable();
					}
					case "license" -> {
						String name = "ARR";
						String url = "https://cdn.modrinth.com/licenses/arr.txt";
						reader.beginObject();
							while ( reader.hasNext() ) {
								switch ( reader.nextName() ) {
									case "name" -> name = reader.nextString();
									case "url" -> url = reader.nextString();
									default -> throw new InvalidMetadataException("Invalid license subkey ${reader.locationString()}!");
								}
							}
						reader.endObject();
						metadata.license = new License( name, url );
					}
					case "icon" -> metadata.icon = reader.nextString();
					case "mixins" -> {
						var mixins = new ArrayList<String>();
						reader.beginArray();
							while ( reader.hasNext() )
								mixins.add( reader.nextString() );
						reader.endArray();
						metadata.mixins =  mixins.immutable();
					}
					case "update" -> {
						var update = new UpdateMetaImpl();
						reader.beginObject();
							while ( reader.hasNext() ) {
								switch ( reader.nextName() ) {
									case "autoUpdate" -> update.autoUpdate = reader.nextBoolean();
									case "modrinth" -> update.modrinth = reader.nextString();
									case "curseforge" -> update.curseforge = reader.nextString();
									default -> throw new InvalidMetadataException("Invalid update subkey ${reader.locationString()}!");
								}
							}
						reader.endObject();
						metadata.update = update;
					}
					case "custom" -> //noinspection unchecked
							metadata.custom = (Map< String, Object >) readCustom( reader );
				}
			}
			reader.endObject();
			return metadata;
		}
	}

	private static Object readCustom(JsonReader reader) {
		switch ( reader.peek() ) {
			case BEGIN_ARRAY -> {
				var list = new ArrayList<>();
				reader.beginArray();
					while ( reader.hasNext() ) {
						list.add(readCustom(reader));
					}
				reader.endArray();
				return list.immutable();
			}
			case BEGIN_OBJECT -> {
				var object = new HashMap<String, Object>();
				reader.beginObject();
					while ( reader.hasNext() ) {
						String key = reader.nextName();
						object.put(key, readCustom(reader));
					}
				reader.endObject();
				return object.immutable();
			}
			case STRING -> {
				return reader.nextString();
			}
			case NUMBER -> {
				return reader.nextDouble();
			}
			case BOOLEAN -> {
				return reader.nextBoolean();
			}
			case NULL -> {
				return null;
				// Unused, probably a sign of malformed json
			}
			default -> throw new IllegalStateException();
		}
	}
}

package com.enderzombi102.endloader.impl.loader.layered;

import com.enderzombi102.endloader.api.EndLoader;
import com.enderzombi102.endloader.api.loader.container.ModContainer;
import com.enderzombi102.endloader.api.loader.layered.LayeredClassLoader;
import com.enderzombi102.endloader.api.loader.layered.TransformingLayer;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.security.CodeSource;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;

/**
 * Layers are the way hot-patching works,
 * Layers are applied in reverse order (LIFO)
 */
public class LayeredClassLoaderImpl extends URLClassLoader implements LayeredClassLoader {
	private static final Logger LOGGER = LoggerFactory.getLogger("EndLoader | LayeredClassLoaderImpl" );
	private final List<TransformingLayer> layers = new ArrayList<>();

	public LayeredClassLoaderImpl() {
		super( "LayeredClassLoader", new URL[] { },  LayeredClassLoaderImpl.class.getClassLoader() );
	}

	@Override
	public void addLayer(TransformingLayer layer) {
		this.layers.add( layer );
	}

	@Override
	protected Class<?> loadClass( String name, boolean resolve ) throws ClassNotFoundException {
		// we can't handle java std classes
		if ( name.startsWith("java") )
			throw new ClassNotFoundException();

		// do we have the requested class?
		var url = this.getResource( name.replace( '.', '/' ) + ".class" );
		if ( url == null )
			throw new ClassNotFoundException(name);

		// yes, load its bytecode
		ByteBuffer bytecode;
		try ( var stream = url.openStream() ) {
			bytecode = ByteBuffer.wrap( stream.readAllBytes() );
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// apply layers
		for ( var layer : layers ) {
			try {
				var bytcode = layer.onClassLoad( name, bytecode );
				// simple check to not screw over
				if ( bytcode != bytecode && bytcode != null )
					bytecode = bytcode;
			} catch (Throwable e) {
				LOGGER.error(
					"Uncaught exception while applying TransformingLayer ${layer.getClass().getSimpleName()} from mod ${LayeredClassLoaderImpl.this.getModByClass( layer.getClass() )}",
					e
				);
			}
		}

		// define and return class
		return this.defineClass( name, bytecode, new CodeSource( url, (Certificate[]) null ) );
	}

	@Override
	public void addURL(URL url) {
		super.addURL( url );
	}

	@Override
	public @Nullable ModContainer getModByClass( Class<?> clazz ) {
		if ( clazz.getName().startsWith("com.enderzombi102.endloader") ) {
			return EndLoader.getLoaderContainer();
		} else if ( clazz.getName().startsWith("net.minecraft") ) {
			return EndLoader.getMinecraftContainer();
		} else {
			var path = Path.of( clazz.getProtectionDomain().getCodeSource().getLocation().toURI() );
			for ( var container : EndLoader.getModContainers(false) ) {
				if ( path.startsWith( container.getJarPath() ) )
					return container;
			}
		}
		return null;
	}


	static {
		registerAsParallelCapable();
	}
}

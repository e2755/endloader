package com.enderzombi102.endloader.impl.loader.meta;

public record License(
		String name,
		String url
) { }

package com.enderzombi102.endloader.api;

import com.enderzombi102.endloader.api.loader.container.ModContainer;
import com.enderzombi102.endloader.impl.Const;
import com.enderzombi102.endloader.impl.loader.EndLoaderImpl;
import com.enderzombi102.endloader.api.loader.layered.LayeredClassLoader;
import com.enderzombi102.endloader.impl.loader.container.BuiltinMods;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
public final class EndLoader {
	public static Path getModsDir() {
		return impl().getModsDir();
	}

	public static Optional<ModContainer> getModContainer( String modid ) {
		return getModContainer( modid, true );
	}

	public static Optional<ModContainer> getModContainer( String modid, boolean includeBuiltin ) {
		return impl().getModContainer( modid, true );
	}

	public static List<ModContainer> getModContainers() {
		return getModContainers(true);
	}

	public static List<ModContainer> getModContainers(boolean includeBuiltin) {
		return impl().getModContainers( includeBuiltin );
	}

	public static Path getMinecraftDir() {
		return impl().getMinecraftDir();
	}

	public static List<ModContainer> getUpdatedMods() {
		return impl().getUpdatedMods();
	}

	public static LayeredClassLoader getLayeredClassLoader() {
		return impl().getLayeredClassLoader();
	}

	public static ModContainer getLoaderContainer() {
		return BuiltinMods.getEndLoader();
	}

	public static ModContainer getMinecraftContainer() {
		return BuiltinMods.getMinecraft();
	}

	public static ModContainer getJavaContainer() {
		return BuiltinMods.getJava();
	}

	public static boolean isDevelopmentEnvironment() {
		return Const.DEV_ENV;
	}

	private static EndLoaderImpl impl() {
		return EndLoaderImpl.INSTANCE;
	}
}

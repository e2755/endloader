package com.enderzombi102.endloader.impl.loader.discovery;

import com.github.zafarkhaja.semver.Version;

import java.nio.file.Path;

public record LoadEnvironment(
	boolean devEnv,
	Path modsDir,
	Version gameVersion
) { }

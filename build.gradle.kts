@file:Suppress("UnstableApiUsage")

plugins {
	id( "com.github.johnrengelman.shadow" ) version "7.1.2"
	id( "org.quiltmc.loom" ) version "0.12-SNAPSHOT"
	`maven-publish`
}

val shade = configurations.create("shade")

loom {
	runConfigs {
//		client {
//			defaultMainClass.set( "com.enderzombi102.endloader.impl.loader.launch.EndClient" )
//			property( "loader.relauncher.disabled", relauncher_disabled )
//		}
//		server {
//			defaultMainClass.set( "com.enderzombi102.endloader.impl.loader.launch.EndServer" )
//			property( "loader.relauncher.disabled", relauncher_disabled )
//		}
	}
}

repositories {
	mavenCentral()
	maven( url="https://maven.fabricmc.net" )
	maven( url="https://repsy.io/mvn/enderzombi102/mc" )
	maven( url="https://oss.sonatype.org/content/repositories/snapshots" )
}

val minecraft_version: String by project
val quilt_mappings: String by project
val relauncher_disabled: String by project

val qjson5_version = "1.0.0"
val joptsimple_version = "5.0.3"
val jsemver_version = "0.10.0-SNAPSHOT"

val asm_version = "9.3"
val tinyparser_version = "0.3.0+build.17"
val tinyremapper_version = "0.6.0"
val aw_version = "2.1.0"
val mixin_version = "0.10.7+mixin.0.8.4"
val manifold_version = "2022.1.23"

dependencies {
	// minecraft
	// To change the versions see the gradle.properties file
	minecraft( "com.mojang:minecraft:$minecraft_version" )
	mappings( "org.quiltmc:quilt-mappings:$minecraft_version+build.$quilt_mappings:v2" )

	shade( api( "org.quiltmc:quilt-json5:$qjson5_version" )!! )
	shade( implementation( "net.sf.jopt-simple:jopt-simple:$joptsimple_version" )!! )
	shade( api( "com.github.zafarkhaja:java-semver:$jsemver_version" )!! )
	// loader dependencies
	api( testAnnotationProcessor( annotationProcessor( "org.ow2.asm:asm:$asm_version" )!! )!! )
	api( testAnnotationProcessor( annotationProcessor( "org.ow2.asm:asm-analysis:$asm_version" )!! )!! )
	api( testAnnotationProcessor( annotationProcessor( "org.ow2.asm:asm-commons:$asm_version" )!! )!! )
	api( testAnnotationProcessor( annotationProcessor( "org.ow2.asm:asm-tree:$asm_version" )!! )!! )
	api( testAnnotationProcessor( annotationProcessor( "org.ow2.asm:asm-util:$asm_version" )!! )!! )
	api( "net.fabricmc:sponge-mixin:$mixin_version" ) {
		exclude( module="launchwrapper" )
		exclude( module="guava" )
	}

	api( "net.fabricmc:tiny-mappings-parser:$tinyparser_version" )
	api( "net.fabricmc:tiny-remapper:$tinyremapper_version" )
	api( "net.fabricmc:access-widener:$aw_version" )

	// -- MANIFOLD --
	// manifold runtimes
	shade( api( "systems.manifold:manifold-rt:$manifold_version" )!! )
	shade( api( "systems.manifold:manifold-ext-rt:$manifold_version" )!! )
	shade( api( "systems.manifold:manifold-collections:$manifold_version" )!! )
	shade( api( "systems.manifold:manifold-io:$manifold_version" )!! )
	shade( api( "systems.manifold:manifold-text:$manifold_version" )!! )
	shade( api( "systems.manifold:manifold-darkj:$manifold_version" )!! )
	// manifold processors
	testAnnotationProcessor( annotationProcessor( "systems.manifold:manifold:$manifold_version" )!! )
	testAnnotationProcessor( annotationProcessor( "systems.manifold:manifold-ext:$manifold_version" )!! )
	testAnnotationProcessor( annotationProcessor( "systems.manifold:manifold-preprocessor:$manifold_version" )!! )
	testAnnotationProcessor( annotationProcessor( "systems.manifold:manifold-exceptions:$manifold_version" )!! )
	testAnnotationProcessor( annotationProcessor( "systems.manifold:manifold-strings:$manifold_version" )!! )
	testAnnotationProcessor( annotationProcessor( "systems.manifold:manifold-collections:$manifold_version" )!! )
	testAnnotationProcessor( annotationProcessor( "systems.manifold:manifold-darkj:$manifold_version" )!! )
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
	configurations.add( shade )
}

val testJar = tasks.create( "testJar", Jar::class ) {
	group = "build"
	description = "Assembles a jar with test sources"
	from( sourceSets.test.get().output )
	dependsOn( tasks.testClasses )
	destinationDirectory.set( project.file("run/mods") )
}
tasks.build.get().dependsOn( testJar )

tasks.withType<JavaCompile> {
	sourceCompatibility = "17" // for the IDE support
	options.encoding = "UTF-8"
	options.release.set(17)
	options.compilerArgs.add( "-Xplugin:Manifold" )
	options.isWarnings = false
}

java {
	withJavadocJar()
	withSourcesJar()
}

tasks.withType<Jar> {
	manifest.attributes( ("Contains-Sources" to "java,class") )

	inputs.property( "version", archiveVersion )
	inputs.property( "minecraft_version", minecraft_version )

	filesMatching("META-INF/endloader.json5") {
		expand(
			"version" to archiveVersion,
			"loader_version" to archiveVersion,
			"minecraft_version" to minecraft_version
		)
	}

	filesMatching("META-INF/minecraft.json5") {
		expand(
			"version" to minecraft_version,
			"loader_version" to archiveVersion,
			"minecraft_version" to minecraft_version
		)
	}
}

publishing {
	publications {
		create<MavenPublication>("mavenJava") {
			from( components["java"] )
		}
	}

	repositories {
		mavenLocal()
	}
}

afterEvaluate {
//	ideaSyncTask.runTask()
}
package com.enderzombi102.endloader.impl.loader.meta;

import java.util.Locale;

public enum Environment {
	SERVER,
	CLIENT,
	BOTH;

	public static Environment of(String value) {
		return switch ( value.toLowerCase(Locale.ROOT) ) {
			default -> BOTH;
			case "client" -> CLIENT;
			case "server" -> SERVER;
		};
	}
}

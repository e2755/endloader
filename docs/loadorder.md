1. Game launched
   1. EndLoader finds mods from `$GAMDE_DIR/mods` folder
   2. EndLoader loads all `endloader.json5` files
   3. Checks for mod updates
      - If any updates are found
        - Downloads new versions
        - Reload `endloader.json5` from new versions
   4. Check if any mod declares an artifact as dependency
      - If any does
        - Collect all artifacts and their dependencies
        - Download all artifacts ( possibly in parallel )
        - Add them to classpath
   5. Load any LanguageAdapters
   6. Checks whether any mod has a pre-relaunch entrypoint
   7. Executes any pre-relaunch entrypoints and gathers additional JVM parameters
   8. Relauncher relaunches the game with new parameters
2. Game Relaunched
   1. EndLoader finds mods from `$GAMDE_DIR/mods` folder
   2. EndLoader loads all `endloader.json5` files
      - Locate and load mixin configs
      - Load any LanguageAdapters that are requested (ie. used)
   3. PreLaunch entrypoint
   4. Initialize mixins
   5. Launch entrypoints
3. Minecraft!
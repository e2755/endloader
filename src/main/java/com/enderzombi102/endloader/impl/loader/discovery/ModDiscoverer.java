package com.enderzombi102.endloader.impl.loader.discovery;

import java.nio.file.Path;
import java.util.List;

public interface ModDiscoverer {
	List<Path> findCandidates( LoadEnvironment env );
}

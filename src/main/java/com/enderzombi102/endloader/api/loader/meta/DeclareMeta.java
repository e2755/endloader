package com.enderzombi102.endloader.api.loader.meta;

import java.util.List;
import java.util.Map;

public interface DeclareMeta {
	Map<String, String> getLanguageAdapters();
	Map<String, String> getEntrypoints();
	List<String> getLayers();
}

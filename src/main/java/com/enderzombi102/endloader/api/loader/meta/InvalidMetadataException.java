package com.enderzombi102.endloader.api.loader.meta;

public class InvalidMetadataException extends Exception {
	public InvalidMetadataException(String msg) {
		super(msg);
	}
}

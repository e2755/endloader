package com.enderzombi102.endloader.impl.loader.layered;

import com.enderzombi102.endloader.api.loader.layered.TransformingLayer;

import java.nio.ByteBuffer;

public class BrandingTransformingLayer implements TransformingLayer {
	@Override
	public ByteBuffer onClassLoad(String clazz, ByteBuffer bytecode) {
		if (! clazz.equals("") )
			return bytecode;
		return bytecode;
	}
}

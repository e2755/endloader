package com.enderzombi102.endloader.impl.loader.container;

import com.enderzombi102.endloader.api.EndLoader;
import com.enderzombi102.endloader.api.ModClassLoader;
import com.enderzombi102.endloader.api.loader.container.ModContainer;
import com.enderzombi102.endloader.api.loader.meta.ModMetadata;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class ModContainerImpl implements ModContainer {
	private final Path path;
	private final ModMetadata metadata;
	private final ModClassLoader classLoader;

	public ModContainerImpl( Path path, ModMetadata metadata ) {
		this.path = path;
		this.metadata = metadata;
		this.classLoader = new ModClassLoaderImpl( this );
	}

	@Override
	public boolean hasBeenUpdated() {
		return EndLoader.getUpdatedMods().contains( this );
	}

	@Override
	public Path getJarPath() {
		return path;
	}

	@Override
	public Path getPath( String part ) {
		return path.resolve( part );
	}

	@Override
	public ModMetadata getMetadata() {
		return this.metadata;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getEntrypoints(Class<T> entrypointClass, String name) {
		return this.metadata.getEntrypoints()
			.getOrDefault( name, List.of() )
			.map( Class::forName )
			.map( x -> (T) x)
			.collect( Collectors.toList() );
	}

	@Override
	public ModClassLoader getModClassLoader() {
		return this.classLoader;
	}

	@Override
	public String toString() {
		return this.metadata.getId();
	}
}

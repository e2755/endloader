package com.enderzombi102.endloader.impl;

import net.minecraft.util.Identifier;

import java.nio.file.Path;
import java.util.function.Supplier;

public class Const {
	public static final Supplier<Path> MC_JAR_PATH = () -> Path.of(
		Identifier.class
			.getProtectionDomain()
			.getCodeSource()
			.getLocation()
			.toURI()
	);
	public static final Supplier<Path> ENDLOADER_JAR_PATH = () -> Path.of(
		Const.class
			.getProtectionDomain()
			.getCodeSource()
			.getLocation()
			.toURI()
	);
	public static final boolean DEV_ENV = Boolean.parseBoolean( System.getProperty("loader.development") );
	public static final Path MC_DIR = Path.of( System.getProperty("user.dir") );
	public static final Path MODS_DIR = MC_DIR.resolve("mods");
}

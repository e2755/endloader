package com.enderzombi102.endloader.impl.loader;

import com.enderzombi102.endloader.api.loader.container.ModContainer;
import com.enderzombi102.endloader.api.loader.layered.LayeredClassLoader;
import com.enderzombi102.endloader.impl.Const;
import com.enderzombi102.endloader.impl.loader.container.BuiltinMods;
import com.enderzombi102.endloader.impl.loader.discovery.LoadEnvironment;
import com.enderzombi102.endloader.impl.loader.discovery.ModFinder;
import com.enderzombi102.endloader.impl.loader.layered.LayeredClassLoaderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EndLoaderImpl {
	private static final Logger LOGGER = LoggerFactory.getLogger("EndLoaderImpl");
	public static final EndLoaderImpl INSTANCE = new EndLoaderImpl();

	private final List<ModContainer> updatedMods = new ArrayList<>();
	private final List<ModContainer> containers = new ArrayList<>();
	private final LayeredClassLoader layeredClassLoader = new LayeredClassLoaderImpl();

	public void init() {

	}

	public Path getModsDir() {
		return Const.MODS_DIR;
	}

	public Path getMinecraftDir() {
		return Const.MC_DIR;
	}

	public List<ModContainer> getUpdatedMods() {
		return updatedMods;
	}

	public LayeredClassLoader getLayeredClassLoader() {
		return layeredClassLoader;
	}

	public Optional<ModContainer> getModContainer( String modid, boolean includeBuiltin ) {
		for ( var container : getModContainers(includeBuiltin) )
			if ( container.getMetadata().getId().equals(modid) )
				return Optional.of( container );
		return Optional.empty();
	}

	public List<ModContainer> getModContainers( boolean includeBuiltin ) {
		return includeBuiltin ? BuiltinMods.addIntegratedMods( containers ).immutable() : containers.immutable();
	}

	// load step *.1 and *.2
	public void findMods() {
		var modJars = new ModFinder().findMods(
			new LoadEnvironment(
				Const.DEV_ENV,
				Const.MODS_DIR,
				BuiltinMods.getMinecraft().getMetadata().getVersion()
			)
		);

	}
}

package com.enderzombi102.endloader.impl.loader.adapter;

import com.enderzombi102.endloader.api.loader.container.ModContainer;
import com.enderzombi102.endloader.api.loader.adapter.LanguageAdapter;
import com.enderzombi102.endloader.api.loader.adapter.LanguageAdapterException;

public class JavaAdapter implements LanguageAdapter {
	@Override
	public <T> T findEntrypoint(Class<T> entrypointType, ModContainer container, String qualifiedName ) throws LanguageAdapterException {
		try {
			//noinspection unchecked
			return (T) container.getModClassLoader().findClass( qualifiedName );
		} catch (ClassNotFoundException e) {
			throw new LanguageAdapterException(e);
		}
	}
}

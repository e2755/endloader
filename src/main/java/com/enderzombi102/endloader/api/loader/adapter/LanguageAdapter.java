package com.enderzombi102.endloader.api.loader.adapter;

import com.enderzombi102.endloader.api.loader.container.ModContainer;

public interface LanguageAdapter {
	<T> T findEntrypoint( Class<T> entrypointType, ModContainer container, String qualifiedName ) throws LanguageAdapterException;
}

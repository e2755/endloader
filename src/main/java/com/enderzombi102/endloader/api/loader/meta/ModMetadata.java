package com.enderzombi102.endloader.api.loader.meta;

import com.enderzombi102.endloader.impl.loader.meta.Environment;
import com.enderzombi102.endloader.impl.loader.meta.License;
import com.github.zafarkhaja.semver.Version;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public interface ModMetadata {
	String getId();
	String getName();
	List<String> getAuthors();
	String getDescription();
	Version getVersion();
	Environment getEnvironment();
	@Nullable String getParent();
	String getLanguage();
	@Nullable DeclareMeta getDeclarations();
	Map< String, List<String> > getEntrypoints();
	List<Dependable> getDependencies();
	Map< String, String > getLinks();
	License getLicense();
	@Nullable String getIcon();
	List<String> getMixins();
	UpdateMeta getUpdate();
	Map< String, ? > getCustom();

	boolean isBuiltin();
}

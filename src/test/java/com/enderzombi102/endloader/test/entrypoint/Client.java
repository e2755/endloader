package com.enderzombi102.endloader.test.entrypoint;

import com.enderzombi102.endloader.api.entrypoint.ClientModInitializer;
import org.slf4j.LoggerFactory;

public class Client implements ClientModInitializer {
	@Override
	public void onClientInitialization() {
		LoggerFactory.getLogger( "Test Mod | " + this.getClass().getSimpleName() ).info("Hello world!");
	}
}

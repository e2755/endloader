package com.enderzombi102.endloader.api.loader.layered;

import com.enderzombi102.endloader.api.loader.container.ModContainer;
import org.jetbrains.annotations.Nullable;

public interface LayeredClassLoader {
	void addLayer( TransformingLayer layer );
	@Nullable ModContainer getModByClass(Class<?> clazz );
}

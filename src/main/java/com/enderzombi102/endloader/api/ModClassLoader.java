package com.enderzombi102.endloader.api;

import com.enderzombi102.endloader.api.loader.container.ModContainer;

public interface ModClassLoader {
	ModContainer getDeclaringMod();
	Class<?> findClass( String name ) throws ClassNotFoundException;
}

package com.enderzombi102.endloader.api.loader.layered;

import java.nio.ByteBuffer;

@FunctionalInterface
public interface TransformingLayer {
	ByteBuffer onClassLoad( String clazz, ByteBuffer bytecode );
}

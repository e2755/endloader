package com.enderzombi102.endloader.impl.loader.container;

import com.enderzombi102.endloader.api.ModClassLoader;
import com.enderzombi102.endloader.api.loader.container.ModContainer;
import com.enderzombi102.endloader.api.loader.meta.ModMetadata;
import com.enderzombi102.endloader.impl.Const;
import com.enderzombi102.endloader.impl.loader.meta.v0.ModMetadataImpl;
import com.enderzombi102.endloader.impl.loader.meta.v0.ModMetadataParser;
import com.github.zafarkhaja.semver.Version;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@SuppressWarnings("ConstantConditions")
public final class BuiltinMods {
	private static final ModContainer MINECRAFT = new BuiltinModContainer(
		Const.MC_JAR_PATH,
		ModMetadataParser.readMetadata( BuiltinMods.class.getResourceAsStream("/META-INF/minecraft.json5") )
	);
	private static final ModContainer ENDLOADER = new BuiltinModContainer(
		Const.ENDLOADER_JAR_PATH,
		ModMetadataParser.readMetadata( BuiltinMods.class.getResourceAsStream("/META-INF/endloader.json5") )
	);
	private static final ModContainer JAVA = new BuiltinModContainer(
		() -> Path.of( System.getProperty("java.home") ),
		new ModMetadataImpl.Builder("java")
			.withName( System.getProperty( "java.vm.name" ) )
			.withAuthors( System.getProperty( "java.vm.vendor" ) )
			.withVersion( Version.valueOf( System.getProperty( "java.vm.version" ) ) )
			.withCustom( Map.of( "javaVersion", System.getProperty( "java.vm.specification.version" ) ) )
			.withLinks( Map.of( "website", System.getProperty( "java.vendor.url" ) ) )
			.withIcon( "endloader:assets/endloader/java_icon.png" )
			.withDescription( "The Java runtime environment" )
			.build()
	);

	public static List<ModContainer> addIntegratedMods( List<ModContainer> mods ) {
		mods.add( MINECRAFT );
		mods.add( ENDLOADER );
		mods.add( JAVA );
		return mods;
	}

	public static ModContainer getEndLoader() {
		return ENDLOADER;
	}

	public static ModContainer getMinecraft() {
		return MINECRAFT;
	}

	public static ModContainer getJava() {
		return JAVA;
	}

	private static class BuiltinModContainer implements ModContainer {

		private final ModClassLoader loader;
		private final Supplier<Path> pathSupplier;
		private final ModMetadata meta;

		public BuiltinModContainer(Supplier<Path> pathSupplier, ModMetadata meta ) {
			this.pathSupplier = pathSupplier;
			this.meta = meta;
			( (ModMetadataImpl) meta ).builtin = true;
			this.loader = new ModClassLoader() {
				@Override
				public ModContainer getDeclaringMod() {
					return BuiltinModContainer.this;
				}

				@Override
				public Class<?> findClass(String name) throws ClassNotFoundException {
					return this.getClass().getClassLoader().loadClass( name );
				}
			};
		}

		@Override
		public boolean hasBeenUpdated() {
			return false;
		}

		@Override
		public Path getJarPath() {
			return this.pathSupplier.get();
		}

		@Override
		public Path getPath(String part) {
			return this.pathSupplier.get().resolve( part );
		}

		@Override
		public ModMetadata getMetadata() {
			return this.meta;
		}

		@Override
		public <T> List<T> getEntrypoints(Class<T> entrypointClass, String name) {
			return List.of();
		}

		@Override
		public ModClassLoader getModClassLoader() {
			return this.loader;
		}

		@Override
		public String toString() {
			return "BuiltinModContainer{modid=${meta.getId()}, path=${pathSupplier.get()}, meta=$meta}";
		}
	}

}

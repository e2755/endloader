package com.enderzombi102.endloader.test.entrypoint;

import com.enderzombi102.endloader.api.entrypoint.ServerModInitializer;
import net.minecraft.server.MinecraftServer;
import org.slf4j.LoggerFactory;

public class Server implements ServerModInitializer {
	@Override
	public void onServerLaunch( MinecraftServer server ) {
		String serverType = server.isDedicated() ? "dedicated" : "integrated";
		LoggerFactory.getLogger( "Test Mod | " + this.getClass().getSimpleName() ).info("Hello $serverType server world!");
	}
}

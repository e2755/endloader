package com.enderzombi102.endloader.test.entrypoint;

import com.enderzombi102.endloader.api.entrypoint.PreRelaunchEntrypoint;
import com.enderzombi102.endloader.api.relauncher.RelaunchContext;
import org.slf4j.LoggerFactory;

public class PreRelaunch implements PreRelaunchEntrypoint {
	@Override
	public void onPreRelaunch( RelaunchContext ctx, boolean gotUpdated ) {
		LoggerFactory.getLogger( "Test Mod | " + this.getClass().getSimpleName() ).info("Hello world!");
	}
}

package com.enderzombi102.endloader.api.entrypoint;

import net.minecraft.server.MinecraftServer;

public interface ServerModInitializer {
	/**
	 * Called on server launch
	 * @param server
	 */
	void onServerLaunch( MinecraftServer server );
}

package com.enderzombi102.endloader.test;

import com.enderzombi102.endloader.api.loader.layered.TransformingLayer;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;

public class TestTransformingLayer implements TransformingLayer {
	@Override
	public ByteBuffer onClassLoad(String clazz, ByteBuffer bytecode) {
		LoggerFactory.getLogger( "test Mod | TestTransformingLayer" ).info("Transforming stuf!");
		return bytecode;
	}
}

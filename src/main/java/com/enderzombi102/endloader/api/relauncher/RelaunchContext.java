package com.enderzombi102.endloader.api.relauncher;

import java.io.File;
import java.nio.file.Path;

public interface RelaunchContext {

	/**
	 * Adds a new file to the new jvm's classpath
	 * @param file file to add
	 */
	void addClasspathEntry( File file );

	/**
	 * Sets the next jvm's path
	 * @param path executable path
	 */
	void setJvmPath( Path path );
}

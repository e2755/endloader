package com.enderzombi102.endloader.impl.loader.meta.v0;

import com.enderzombi102.endloader.api.loader.meta.DeclareMeta;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class DeclareMetaImpl implements DeclareMeta {
	Map< String, String > languageAdapters;
	Map< String, String > entrypoints;
	List< String> layers;

	DeclareMetaImpl( Map< String, String > languageAdapters, Map< String, String > entrypoints, List<String> layers ) {
		this.languageAdapters = Collections.unmodifiableMap( languageAdapters );
		this.entrypoints = Collections.unmodifiableMap( entrypoints );
		this.layers = Collections.unmodifiableList( layers );
	}

	@Override
	public Map<String, String> getLanguageAdapters() {
		return languageAdapters;
	}

	@Override
	public Map<String, String> getEntrypoints() {
		return languageAdapters;
	}

	@Override
	public List<String> getLayers() {
		return layers;
	}

	@Override
	public String toString() {
		return "DeclareMetaImpl{languageAdapters=$languageAdapters, entrypoints=$entrypoints, layers=$layers}";
	}
}

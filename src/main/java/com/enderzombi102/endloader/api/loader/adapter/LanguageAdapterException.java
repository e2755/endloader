package com.enderzombi102.endloader.api.loader.adapter;

/**
 * Exception thrown by LanguageAdapters
 */
public class LanguageAdapterException extends Exception {
	public LanguageAdapterException(String message) {
		super( message );
	}

	public LanguageAdapterException(String message, Throwable cause ) {
		super( message, cause );
	}

	public LanguageAdapterException(Throwable cause ) {
		super( cause );
	}

	protected LanguageAdapterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace ) {
		super( message, cause, enableSuppression, writableStackTrace );
	}
}

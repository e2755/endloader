package com.enderzombi102.endloader.impl.loader.meta.v0;

import com.enderzombi102.endloader.api.EndLoader;
import com.enderzombi102.endloader.api.loader.meta.DeclareMeta;
import com.enderzombi102.endloader.api.loader.meta.Dependable;
import com.enderzombi102.endloader.api.loader.meta.ModMetadata;
import com.enderzombi102.endloader.api.loader.meta.UpdateMeta;
import com.enderzombi102.endloader.impl.loader.meta.Environment;
import com.enderzombi102.endloader.impl.loader.meta.License;
import com.github.zafarkhaja.semver.Version;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ModMetadataImpl implements ModMetadata {
	String id;
	String name;
	List<String> authors;
	String description;
	Version version;
	Environment environment;
	@Nullable String parent;
	String language;
	@Nullable DeclareMeta declare;
	Map< String, List<String> > entrypoints;
	List<Dependable> dependencies;
	Map< String, String > links;
	License license;
	@Nullable String icon;
	List<String> mixins;
	UpdateMeta update;
	Map< String, ? > custom;
	public boolean builtin = false;

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<String> getAuthors() {
		return this.authors;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public Version getVersion() {
		return this.version;
	}

	@Override
	public Environment getEnvironment() {
		return this.environment;
	}

	@Override
	public @Nullable String getParent() {
		return this.parent;
	}

	@Override
	public String getLanguage() {
		return this.language;
	}

	@Override
	public @Nullable DeclareMeta getDeclarations() {
		return this.declare;
	}

	@Override
	public Map<String, List<String>> getEntrypoints() {
		return this.entrypoints;
	}

	@Override
	public List<Dependable> getDependencies() {
		return this.dependencies;
	}

	@Override
	public Map<String, String> getLinks() {
		return this.links;
	}

	@Override
	public License getLicense() {
		return this.license;
	}

	@Override
	public @Nullable String getIcon() {
		return this.icon;
	}

	@Override
	public List<String> getMixins() {
		return this.mixins;
	}

	@Override
	public UpdateMeta getUpdate() {
		return this.update;
	}

	@Override
	public Map<String, ?> getCustom() {
		return this.custom;
	}

	@Override
	public boolean isBuiltin() {
		return this.builtin;
	}

	@Override
	public String toString() {
		return "ModMetadataImpl{" +
				"name='$name'" +
				", authors=$authors" +
				", description='$description'" +
				", version=$version" +
				", environment=$environment" +
				", parent='$parent'" +
				", language='$language'" +
				", declare=$declare" +
				", entrypoints=$entrypoints" +
				", dependencies=$dependencies" +
				", links=$links" +
				", license=$license" +
				", icon='$icon'" +
				", mixins=$mixins" +
				", update=$update" +
				", custom=$custom" +
				", builtin=$builtin}";
	}

	public static final class Builder {
		private final ModMetadataImpl meta;

		public Builder(String modid) {
			this.meta = new ModMetadataImpl();
			this.meta.id = modid;
			this.meta.name = modid;
			this.meta.authors = List.of();
			this.meta.description = "A builtin mod";
			this.meta.version = Version.valueOf("1.0.0");
			this.meta.environment = Environment.BOTH;
			this.meta.parent = "";
			this.meta.language = "java";
			this.meta.declare = new DeclareMetaImpl( Map.of(), Map.of(), List.of() );
			this.meta.entrypoints = Map.of();
			this.meta.dependencies = List.of(
				new Dependency("endloader", EndLoader.getLoaderContainer().getMetadata().getVersion().toString() ),
				new Dependency("minecraft", EndLoader.getMinecraftContainer().getMetadata().getVersion().toString() )
			);
			this.meta.links = Map.of();
			this.meta.license = new License("ARR", "https://cdn.modrinth.com/licenses/arr.txt");
			this.meta.icon = null;
			this.meta.mixins = List.of();
			this.meta.update = new UpdateMetaImpl(false);
			this.meta.custom = Map.of();
		}

		public Builder withName(String name) {
			this.meta.name = name;
			return this;
		}

		public Builder withAuthors(List<String> authors) {
			this.meta.authors = Collections.unmodifiableList( authors );
			return this;
		}

		public Builder withAuthors(String authors) {
			this.meta.authors = Collections.<String>unmodifiableList( authors.split( "," ).toList() );
			return this;
		}

		public Builder withDescription(String description) {
			this.meta.description = description;
			return this;
		}

		public Builder withVersion(Version version) {
			this.meta.version = version;
			return this;
		}

		public Builder withEnvironment(Environment environment) {
			this.meta.environment = environment;
			return this;
		}

		public Builder withParent(String parent) {
			this.meta.parent = parent;
			return this;
		}

		public Builder withLanguage(String language) {
			this.meta.language = language;
			return this;
		}

		public Builder withDeclare(DeclareMeta declare) {
			this.meta.declare = declare;
			return this;
		}

		public Builder withEntrypoints(Map<String, List<String>> entrypoints) {
			this.meta.entrypoints = Collections.unmodifiableMap( entrypoints );
			return this;
		}

		public Builder withDependencies(List<Dependable> dependencies) {
			this.meta.dependencies = Collections.unmodifiableList( dependencies );
			return this;
		}

		public Builder withDependencies(Dependable... dependencies) {
			this.meta.dependencies = Collections.<Dependable>unmodifiableList( dependencies.toList() );
			return this;
		}

		public Builder withLinks(Map<String, String> links) {
			this.meta.links = Collections.unmodifiableMap( links );
			return this;
		}

		public Builder withLicense(License license) {
			this.meta.license = license;
			return this;
		}

		public Builder withIcon(String icon) {
			this.meta.icon = icon;
			return this;
		}

		public Builder withMixins(List<String> mixins) {
			this.meta.mixins = Collections.unmodifiableList( mixins );
			return this;
		}

		public Builder withUpdate(UpdateMeta update) {
			this.meta.update = update;
			return this;
		}

		public Builder withCustom(Map<String, ?> custom) {
			this.meta.custom = Collections.unmodifiableMap( custom );
			return this;
		}

		public ModMetadata build() {
			return this.meta;
		}
	}
}

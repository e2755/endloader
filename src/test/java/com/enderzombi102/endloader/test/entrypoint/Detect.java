package com.enderzombi102.endloader.test.entrypoint;

import com.enderzombi102.endloader.api.loader.container.ModContainer;
import org.slf4j.LoggerFactory;

public class Detect {
	public void onInitialize( ModContainer container ) {
		LoggerFactory.getLogger( "Test Mod | " + this.getClass().getSimpleName() ).info("Hello world!");
	}
}

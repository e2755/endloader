pluginManagement {
	repositories {
		mavenCentral()
		gradlePluginPortal()
		maven( url="https://maven.fabricmc.net" )
		maven( url="https://maven.quiltmc.org/repository/release" )
		maven( url="https://maven.quiltmc.org/repository/snapshot" )
	}
}

rootProject.name = "EndLoader"
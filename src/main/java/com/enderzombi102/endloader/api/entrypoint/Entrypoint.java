package com.enderzombi102.endloader.api.entrypoint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate an entrypoint class or method
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Entrypoint {
	/**
	 * This value may be used to specify what entrypoint type this is if it can't be detected.
	 * This must be only used if annotating a method.
	 */
	String value = "DETECT";
}

package com.enderzombi102.endloader.api.loader.meta;

import org.jetbrains.annotations.Nullable;

public interface UpdateMeta {
	boolean autoUpdateEnabled();
	@Nullable String getModrinthID();
	@Nullable String getCurseforgeID();
}

package com.enderzombi102.endloader.api.loader.meta;

import com.enderzombi102.endloader.impl.loader.discovery.LoadEnvironment;

public interface Dependable {
	boolean acceptsEnvironment( LoadEnvironment environment );

	String getName();
	String getVersion();
}

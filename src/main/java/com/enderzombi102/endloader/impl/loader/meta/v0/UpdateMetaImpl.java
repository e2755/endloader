package com.enderzombi102.endloader.impl.loader.meta.v0;

import com.enderzombi102.endloader.api.loader.meta.UpdateMeta;
import org.jetbrains.annotations.Nullable;

public class UpdateMetaImpl implements UpdateMeta {
	boolean autoUpdate = false;
	@Nullable String modrinth = null;
	@Nullable String curseforge = null;

	public UpdateMetaImpl() { }
	public UpdateMetaImpl(boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}

	@Override
	public boolean autoUpdateEnabled() {
		return autoUpdate;
	}

	@Override
	public @Nullable String getModrinthID() {
		return modrinth;
	}

	@Override
	public @Nullable String getCurseforgeID() {
		return curseforge;
	}

	@Override
	public String toString() {
		return "UpdateMetaImpl{autoUpdate=$autoUpdate, modrinth='$modrinth', curseforge='$curseforge'}";
	}
}

package com.enderzombi102.endloader.impl.loader.meta.v0;

import com.enderzombi102.endloader.api.loader.meta.Dependable;
import com.enderzombi102.endloader.impl.loader.discovery.LoadEnvironment;

public record Dependency(
		String modid,
		String version
) implements Dependable {
	@Override
	public boolean acceptsEnvironment( LoadEnvironment environment ) {
		return true;
	}

	@Override
	public String getName() {
		return this.modid;
	}

	@Override
	public String getVersion() {
		return this.version;
	}

	@Override
	public String toString() {
		return "Dependency{modid='$modid'version='$version'}";
	}
}

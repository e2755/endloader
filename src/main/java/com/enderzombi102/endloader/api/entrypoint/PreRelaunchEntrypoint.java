package com.enderzombi102.endloader.api.entrypoint;

import com.enderzombi102.endloader.api.relauncher.RelaunchContext;

public interface PreRelaunchEntrypoint {
	/**
	 * Executed on starting JVM, may be used to alter the game's JVM start parameters or classpath
	 * @param ctx Relaunch Context to modify the relauncher behavior
	 * @param gotUpdated if gotUpdated is true, the mod has been updated and the loaded jar is still the old in-memory one
	 */
	void onPreRelaunch( RelaunchContext ctx, boolean gotUpdated );
}
